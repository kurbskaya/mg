#include <limits> 
#include <cmath> 
#include <iostream> 
#include <fstream> 
#include <vector> 
#include <cstring> 
#include <sstream>
#include <cassert>
#include <omp.h>
#include <unordered_map>
#include "head.h"
#include "Bitmap.h"

const int NUM = 512;
const int REK = 3;

using namespace std;

struct Pixel { unsigned char r, g, b; };

void WriteBMP(const char* fname, Pixel* a_pixelData, int width, int height)
{
  int paddedsize = (width*height) * sizeof(Pixel);

  unsigned char bmpfileheader[14] = {'B','M', 0,0,0,0, 0,0, 0,0, 54,0,0,0};
  unsigned char bmpinfoheader[40] = {40,0,0,0, 0,0,0,0, 0,0,0,0, 1,0, 24,0};

  bmpfileheader[ 2] = (unsigned char)(paddedsize    );
  bmpfileheader[ 3] = (unsigned char)(paddedsize>> 8);
  bmpfileheader[ 4] = (unsigned char)(paddedsize>>16);
  bmpfileheader[ 5] = (unsigned char)(paddedsize>>24);

  bmpinfoheader[ 4] = (unsigned char)(width    );
  bmpinfoheader[ 5] = (unsigned char)(width>> 8);
  bmpinfoheader[ 6] = (unsigned char)(width>>16);
  bmpinfoheader[ 7] = (unsigned char)(width>>24);
  bmpinfoheader[ 8] = (unsigned char)(height    );
  bmpinfoheader[ 9] = (unsigned char)(height>> 8);
  bmpinfoheader[10] = (unsigned char)(height>>16);
  bmpinfoheader[11] = (unsigned char)(height>>24);

  ofstream out(fname, ios::out | ios::binary);
  out.write((const char*)bmpfileheader, 14);
  out.write((const char*)bmpinfoheader, 40);
  out.write((const char*)a_pixelData, paddedsize);
  out.flush();
  out.close();
}

void SaveBMP(const char* fname, const unsigned int* pixels, int w, int h)
{
  vector<Pixel> pixels2(w*h);

  for (size_t i = 0; i < pixels2.size(); i++)
  {
    Pixel px;
    px.r       = (pixels[i] & 0x00FF0000) >> 16;
    px.g       = (pixels[i] & 0x0000FF00) >> 8;
    px.b       = (pixels[i] & 0x000000FF);
    pixels2[i] = px;
  }

  WriteBMP(fname, &pixels2[0], w, h);
}

class Kube {
    float x1;
    float x2;
    float x3;
    float x4;
    float x5;
    float x6;

public :

    vector<Vec3i> figures;

    Kube() : x1(0), x2(0), x3(0), x4(0), x5(0), x6(0), figures() {}
    Kube(const float &x1, const float &x2, const float &x3, const float &x4, const float &x5, const float &x6) : x1(x1), x2(x2), x3(x3), x4(x4), x5(x5), x6(x6), figures() {}

    bool ray_kube_intersect(const Vec3f &orig, const Vec3f &dir) const {
        float tmin, tmax;
        float lo = 1. / dir.x * (x1 - orig.x);
        float hi = 1. / dir.x * (x4 - orig.x);
        tmin  = min(lo, hi);
        tmax = max(lo, hi);

        float lo1 = 1. / dir.y * (x2 - orig.y);
        float hi1 = 1. / dir.y * (x5 - orig.y);
        tmin  = max(tmin, min(lo1, hi1));
        tmax = min(tmax, max(lo1, hi1));

        float lo2 = 1. / dir.z * (x3 - orig.z);
        float hi2 = 1. / dir.z * (x6 - orig.z);
        tmin  = max(tmin, min(lo2, hi2));
        tmax = min(tmax, max(lo2, hi2));

        return (tmin <= tmax) && (tmax > 0.f);
    }

    vector<Kube> kub_size_expr() const {
        vector<Kube> k;
        k.push_back(Kube(x1 , (x2 + x5)/2.0 , (x3 + x6)/2.0 , (x1 + x4)/2.0 , x5 , x6 ));
        k.push_back(Kube(x1 , (x2 + x5)/2.0 , x3 , (x1 + x4)/2.0 , x5 , (x3 + x6)/2.0 ));

        k.push_back(Kube((x1 + x4)/2.0 , (x2 + x5)/2.0 , (x3 + x6)/2.0 , x4 , x5 , x6 ));
        k.push_back(Kube((x1 + x4)/2.0 , (x2 + x5)/2.0 , x3 , x4 , x5 , (x3 + x6)/2.0 ));

        k.push_back(Kube(x1 , x2 , (x3 + x6)/2.0 , (x1 + x4)/2.0 , (x5 + x2)/2.0 , x6 ));
        k.push_back(Kube(x1 , x2 , x3 , (x1 + x4)/2.0 , (x5 + x2)/2.0 , (x3 + x6)/2.0 ));

        k.push_back(Kube((x1 + x4)/2.0 , x2 , (x3 + x6)/2.0 , x4 , (x2 + x5)/2.0 , x6 ));
        k.push_back(Kube((x1 + x4)/2.0 , x2 , x3 , x4 , (x2 + x5)/2.0 , (x3 + x6)/2.0 ));

       return k;
    }
    bool is_in(const Vec3f &point) const{
        if((x1 <= point.x) && (x4 >= point.x) && (x2 <= point.y) && (x5 >= point.y) && (x3 <= point.z) && (x6 >= point.z))
            return true;
        else
            return false;
    }
    int coord() const {
        float a_x = (x4 + x1) / 2;
        float a_y = (x5 + x2) / 2;
        float a_z = (x6 + x3) / 2;
        float a = (a_x * a_x + a_y * a_y + a_z * a_z) * 1000;
        int b = (int)a;
        while (b > 100000){
            b = b-100000;
        } 
        return b;


    }
    bool empty_not(){
        if (x1 || x2 || x3 || x4 || x5 || x6)
            return true;
        else
            return false;
    }
    bool kub_tri_intersect(const Vec3f &v1, const Vec3f &v2, const Vec3f &v3){
        float x1 = min(v1.x, min (v2.x, v3.x));
        float x2 = min(v1.y, min (v2.y, v3.y));
        float x3 = min(v1.z, min (v2.z, v3.z));
        float x4 = max(v1.x, max (v2.x, v3.x));
        float x5 = max(v1.y, max (v2.y, v3.y));
        float x6 = max(v1.z, max (v2.z, v3.z));
        if ((*this).x1 > x4 || (*this).x2 > x5 || (*this).x3 > x6 || (*this).x4 < x1 || (*this).x5 < x2 || (*this).x6 < x3) 
            return false;
        else
            return true;
    }

    friend ostream& operator<<(ostream& out, const Kube &m) {
        out << m.x1 << " " << m.x2 << " " << m.x3 << " " << m.x4 << " " << m.x5 << " " << m.x6 << endl;
    return out;
    }
};

void rek_devide(const Kube &kub, vector <Kube> &sq, int depth=REK){
    vector<Kube> k1;
    if(depth == 0){
        sq.push_back(kub);
        return;
    }else{
        k1 = kub.kub_size_expr();
        depth--;
        for(int i = 0; i < 8; i++){
            rek_devide(k1[i], sq, depth);
        }
        return;
    }
}


class Model {
    vector<Vec3f> verts;
    vector<Vec3i> faces;
public:

    Kube kube;
    Kube massive[100000] = {};
	//Material material;

    Model() : verts(), faces(), kube(), massive() {}

    Model(const char *filename) : verts(), faces(), kube(), massive() {
        ifstream in;
        in.open (filename, ifstream::in);
        if (in.fail()) {
            cerr << "Failed to open " << filename << endl;
            return;
        }
        string line;
        while (!in.eof()) {
            getline(in, line);
            istringstream iss(line.c_str());
            char trash;
            if (!line.compare(0, 2, "v ")) {
                iss >> trash;
                Vec3f v;
                for (int i=0;i<3;i++) {
                    iss >> v[i];
                    v[i] /= 10.;
                }
                v[0] += 13.;
                v[1] += 8.;
                v[2] -= 30.;
                verts.push_back(v);
            } else if (!line.compare(0, 2, "f ")) {
                Vec3i f;
                int idx, cnt=0;
                iss >> trash;
                while (iss >> idx) {
                    idx--;
                    f[cnt++] = idx;
                }
                if (3==cnt) faces.push_back(f);
            }
        }

        Vec3f min, max;
        kube = get_bbox(min, max);
    }

    int nverts() const {                          
        return (int)verts.size();
    }

    int nfaces() const {                          
        return (int)faces.size();
    }

    
    bool ray_triangle_intersect(const Vec3f &v0, const Vec3f &v1, const Vec3f &v2, const Vec3f &orig, const Vec3f &dir, float &tnear) const {
        Vec3f edge1 = v1 - v0;
        Vec3f edge2 = v2 - v0;
        Vec3f pvec = cross(dir, edge2);
        float det = edge1 * pvec;
        if (det < 1e-5) return false;

        Vec3f tvec = orig - v0;
        float u = tvec*pvec;
        if (u < 0 || u > det) return false;

        Vec3f qvec = cross(tvec, edge1);
        float v = dir*qvec;
        if (v < 0 || u + v > det) return false;

        tnear = edge2*qvec * (1./det);
        return tnear>1e-5;
    }

    const Vec3f &point(int i) const {                   
        assert(i >= 0 && i < nverts());
        return verts[i];
    }

    Vec3f &point(int i) {                   
        assert(i >= 0 && i < nverts());
        return verts[i];
    }

    Vec3i &face(int i)  {                   
        assert(i >= 0 && i < nfaces());
        return faces[i];
    }

    int vert(int fi, int li) const {              
        assert(fi>=0 && fi<nfaces() && li>=0 && li<3);
        return faces[fi][li];
    }

    Kube get_bbox(Vec3f &min, Vec3f &max) { 
        min = max = verts[0];
        for (int i=1; i<(int)verts.size(); ++i) {
            for (int j=0; j<3; j++) {
                min[j] = std::min(min[j], verts[i][j]);
                max[j] = std::max(max[j], verts[i][j]);
            }
        }
        return Kube(min[0], min[1], min[2], max[0], max[1], max[2]);
    }            
};

void rek_intersect(const Vec3f &orig, const Vec3f &dir, const Kube &kub, vector <Vec3i> &sq, const Model &model, int depth = REK){
    vector<Kube> k1;
    vector<Vec3i> tri;

    float models_dist = numeric_limits<float>::max();
    if (depth == 0) {
        int ind = -1;
        tri = model.massive[kub.coord()].figures;
        for(size_t i=0; i<tri.size(); i++){
            float dist=0;
            Vec3f v0 = model.point(tri[i].x);
            Vec3f v1 = model.point(tri[i].y);
            Vec3f v2 = model.point(tri[i].z);
            if (model.ray_triangle_intersect(v0, v1, v2, orig, dir, dist) && dist<models_dist) {
                models_dist = dist;
                ind = i;
            }
        }
        if (ind != -1) {
            sq.push_back(tri[ind
                ]);
        }
        return;
    }else{
        depth--;
        k1 = kub.kub_size_expr();
        for (int i = 0; i < 8; i++){
            if (k1[i].ray_kube_intersect(orig, dir)){
                rek_intersect(orig, dir, k1[i], sq, model, depth);
            }
        }
        return;
    }
}

void basa( Model &model){
    vector <Kube> sq;
    Kube tmp = model.kube;
    rek_devide(tmp, sq);
    for (int i=0; i<model.nfaces(); i++){
        for (int k=0; k < NUM; k++){
            if (sq[k].kub_tri_intersect(model.point(model.vert(i, 0)), model.point(model.vert(i, 1)), model.point(model.vert(i, 2)))){
                sq[k].figures.push_back(model.face(i));
                model.massive[sq[k].coord()] = sq[k];
            }
        } 
    }
    return;
}

ostream& operator<<(ostream& out, Model &m) {
    for (int i=0; i<m.nverts(); i++) {
        out << "v " << m.point(i) << endl;
    }
    for (int i=0; i<m.nfaces(); i++) {
        out << "f ";
        for (int k=0; k<3; k++) {
            out << (m.vert(i,k)+1) << " ";
        }
        out << endl;
    }
    return out;
}

struct Material{
	float refractive_index;
	Vec4f albedo;
	Vec3f diffuse_color;
	float specular_exponent;

	Material(const float r,const Vec4f &a, const Vec3f &color, const float spec):refractive_index(r), albedo(a), diffuse_color(color), specular_exponent(spec){}
	Material() : refractive_index(1), albedo(1,0,0,0), diffuse_color(), specular_exponent(){}
};

struct Sphere{
	Vec3f center;
	float radius;
	Material material;

	Sphere(const Vec3f &c, const float r, const Material &m) : center(c), radius(r), material(m) {}

	bool ray_intersect(const Vec3f &orig, const Vec3f &dir, float &t0) const {
		Vec3f L = center-orig;
		float tca = L*dir;
		float d2 = L*L-tca*tca;
		if (d2> radius*radius) return false;
		float thc = sqrtf(radius*radius-d2);
		t0 = tca-thc;
		float t1 = tca+thc;
		if (t0<0) t0=t1;
		if (t0<0) return false;
		return true;
	}
};

struct Light{
	Vec3f position;
	float intensity;

	Light(const Vec3f &p, const float i): position(p), intensity(i){}
};

Vec3f reflect(const Vec3f &I, const Vec3f &N){ return I - N * 2.f * (I * N); }

Vec3f refract(const Vec3f &I, const Vec3f &N, const float &refractive_index) { 
    float cosi = - max(-1.f, min(1.f, I * N));
    float etai = 1, etat = refractive_index;
    Vec3f n = N;
    if (cosi < 0) { 
        cosi = -cosi;
        swap(etai, etat);
        n = -N;
    }
    float eta = etai / etat;
    float k = 1 - eta * eta * (1 - cosi * cosi);
    return k < 0 ? Vec3f(0, 0, 0) : I * eta + n * (eta * cosi - sqrtf(k));
}

bool scene_intersect(const Vec3f &orig, const Vec3f &dir, const vector<Sphere> &spheres, Vec3f &hit, Vec3f &N, Material &material,const Model &model, int arg){
	float spheres_dist = numeric_limits<float>::max();
	for (size_t i=0; i<spheres.size(); i++){
		float dist_i=0;
		if (spheres[i].ray_intersect(orig, dir, dist_i) && (dist_i < spheres_dist)){
			spheres_dist = dist_i;
			hit = orig + dir*dist_i;
			N = (hit - spheres[i].center).normalize();
			material = spheres[i].material;
		}
	}


	float models_dist = numeric_limits<float>::max();
    vector <Vec3i> sq;

if (arg==2){
   
    if(model.kube.ray_kube_intersect(orig,dir)){
        rek_intersect(orig, dir, model.kube, sq, model);
        for (size_t t=0; t<sq.size(); t++) {
            float dist=0;
            Vec3f v0 = model.point(sq[t].x);
            Vec3f v1 = model.point(sq[t].y);
            Vec3f v2 = model.point(sq[t].z);
            if (model.ray_triangle_intersect(v0, v1, v2, orig, dir, dist) && dist<models_dist && dist<spheres_dist) {
                models_dist = dist;
                hit = orig + dir * dist;
                N = cross(v1-v0, v2-v0).normalize();
                if (N * dir > 0.) {
                    N = -N;
                }
                material =  Material(1.5, Vec4f(0.3,  1.5, 0.2, 0.5), Vec3f(.24, .21, .09),  125.);
                material = Material(1.0, Vec4f (0.9, 0.1, 0.0, 0.0), Vec3f(0.1, 0.1, 0.1), 10.);
            }
        }
        
   }
}

	float checkboard1_dist = numeric_limits<float>::max();
    if ((arg==2)||(arg==3)){
	   if (fabs(dir.y)>1e-3){
	       float d = -(orig.y+4)/dir.y;
		      Vec3f pt = orig + dir*d;
		      if (d>0  && d < spheres_dist && d < models_dist) {
			     checkboard1_dist = d;
			     hit = pt;
			     N = Vec3f(0, 1, 0);
			     material = Material();
			     material.diffuse_color = (int(.5*hit.x+1000)+int(.5*hit.z)) & 1 ? Vec3f(1 ,1 ,1) : Vec3f(0, 0, 0);
			     material.diffuse_color = material.diffuse_color*.3;
		      }
	   }
    }
	float checkboard2_dist = numeric_limits<float>::max();
	if (fabs(dir.x)>1e-3){
		float d = -(orig.x+25)/dir.x;
		Vec3f pt = orig + dir*d;
		if (d>0 && (pt.z)<0  && pt.z>-30 && fabs(pt.y)<10 && d<spheres_dist &&  d < models_dist && d<checkboard1_dist) {
			checkboard2_dist = d;
			hit = pt;
			N = Vec3f(1, 0, 0);
			material = Material();
			material.diffuse_color =  Vec3f(0.1,0.4,0.4);
			material.diffuse_color = material.diffuse_color*.3;
		}
	}
	return min(models_dist, min(spheres_dist,   min(checkboard1_dist, checkboard2_dist)))<1000;	
}

Vec3f cast_ray(const Vec3f &orig, const Vec3f &dir, const vector<Sphere> &spheres, const vector<Light> &lights, const vector<Vec3f> &envmap, const int e_w, const int e_h, const Model &duck,const int &arg, size_t depth=0){
	Vec3f point, N;
	Material material;
	if(arg==1 && (depth> 3|| !scene_intersect(orig, dir, spheres, point, N, material, duck,arg))){

		Sphere env(Vec3f(0,0,0), 1000, Material());
        float dist = 0;
        env.ray_intersect(orig, dir, dist);
        Vec3f p = orig+dir*dist;
        size_t a = (atan2(p.z, p.x)/(2*M_PI) + .5)*e_w;
        size_t b = acos(p.y/1000)/M_PI*e_h;
        if (a+b*e_w < envmap.size())
            return envmap[a+b*e_w];
        else
		  return Vec3f(0.9, 0.0, 0.8);
	}

    if((arg==2 || arg==3) && (depth> 3|| !scene_intersect(orig, dir, spheres, point, N, material, duck, arg)))
          return Vec3f(0.9, 0.9, 0.6);



	Vec3f reflect_dir = reflect(dir, N).normalize();
	Vec3f refract_dir = refract(dir, N, material.refractive_index).normalize();
	Vec3f reflect_orig = reflect_dir*N<0 ? point-N*(1e-3) : point+N*(1e-3);
	Vec3f refract_orig = refract_dir*N<0 ? point-N*(1e-3) : point+N*(1e-3);
	Vec3f reflect_color = cast_ray(reflect_orig, reflect_dir, spheres, lights, envmap, e_w, e_h, duck, arg, depth+1);
	Vec3f refract_color = cast_ray(refract_orig, refract_dir, spheres, lights, envmap, e_w, e_h, duck, arg, depth+1);


	float diffuse_light_intensity = 0, specular_light_intensity = 0;
	for (size_t i=0; i<lights.size(); i++){
		Vec3f light_dir=(lights[i].position-point).normalize();
		float light_distance = (lights[i].position-point).norm();

		Vec3f shadow_orig = light_dir*N<0 ? point-N*(1e-3) : point+N*(1e-3);
		Vec3f shadow_pt, shadow_N;
		Material tmpmaterial;
		if (scene_intersect(shadow_orig, light_dir, spheres, shadow_pt, shadow_N, tmpmaterial, duck, arg) && (shadow_pt-shadow_orig).norm()<light_distance){
			continue;
		}
        float coef = 1. / ((lights[i].position - point) * (lights[i].position - point));
		diffuse_light_intensity += lights[i].intensity * max(0.f, light_dir*N) * coef;
		specular_light_intensity += powf(max(0.f,-reflect(-light_dir, N)*dir), material.specular_exponent)*lights[i].intensity * coef;
	}
	return material.diffuse_color * diffuse_light_intensity * material.albedo[0]+Vec3f(1.,1.,1.)*specular_light_intensity*material.albedo[1]+reflect_color*material.albedo[2]+refract_color*material.albedo[3];
}


void render(const vector<Sphere> &spheres, const vector<Light> &lights, const Model &duck, const string &outFilePath, const int &arg){
	
	const int width=2048;
	const int height=1152;
	const int fov=M_PI/3.;
   

    int e_w = 0;
    int e_h = 0;
    vector<Vec3f> envmap(e_w*e_h);
    
    if (arg==1){
        ifstream ifs;
        ifs.open ("../sea.ppm");
        string tmp;
    
        ifs >> tmp >> e_w >> e_h >> tmp;
        vector<Vec3f> envmap1(e_w*e_h);
        envmap=envmap1;

        unsigned char c;
        ifs >> std::noskipws >> c; 
        for (int i = 0; i < e_h * e_w; ++i) {
            vector<float> vc(3);
            for (size_t j = 0; j < 3; j++) {
                ifs >> noskipws >> c;
                vc[j] = (float)c * (1 / 255.);
            }
            envmap[i] = Vec3f(vc[0], vc[1], vc[2]);
        }
        ifs.close();
    }
    else if((arg==2)||(arg==3)){
        e_w = e_h = 0 ;
    }
    vector <Vec3f> framebuffer(width*height);
    #pragma omp parallel for num_threads(16)


	for (size_t j=0; j<height; j++) {
		for (size_t i=0; i<width; i++) {
			float x = (2 * (i + 0.5) / (float)width - 1) * tan(fov / 2.) * width / (float)height;
			float y = - (2 * (j + 0.5) / (float)height - 1) * tan(fov / 2.);
			Vec3f dir = Vec3f(x, y , -1).normalize();

	 		framebuffer[i+(height - j - 1)*width]=cast_ray(Vec3f(0,0,30),  dir, spheres, lights, envmap, e_w, e_h, duck, arg);
	 
		}
	}


    vector<uint32_t> image(width * height); 
    
    for (size_t i=0; i<height*width; ++i) {
        image[i] = 0;
        for (size_t j=0; j<3; j++) {
            image[i] = image[i] | (unsigned)(255*max(0.f, min(1.f, framebuffer[i][2 - j])));
            if (j != 2)
                image[i] <<= 8;
        }
    }

    SaveBMP(outFilePath.c_str(), image.data(), width, height);
}


int main(int argc, char *argv[]){
    unordered_map<string, string> cmdLineParams;
    for(int i=0; i<argc; i++)
    {
        string key(argv[i]);

        if(key.size() > 0 && key[0]=='-')
        {
            if(i != argc-1)
            {
                cmdLineParams[key] = argv[i+1];
                i++;
            }
            else
                cmdLineParams[key] = "";
        }
    }

    string outFilePath = "./out2.bmp";
    if(cmdLineParams.find("-out") != cmdLineParams.end()){
        outFilePath = cmdLineParams["-out"];
    }

    int sceneId = 0;
    if(cmdLineParams.find("-scene") != cmdLineParams.end()){
        sceneId = atoi(cmdLineParams["-scene"].c_str());
    }

  
    Material mirror (1.0, Vec4f (0.0, 10.0, 0.8, 0.0), Vec3f(1.0, 1.0, 1.0), 1425.);
    Material glass(1.5, Vec4f(0.0, 0.5, 0.1, 0.8), Vec3f(0.6, 0.7, 0.8), 125.);
	Material pudra(1.5, Vec4f(0.6, 0.3, 0.1, 0.0), Vec3f(0.4, 0.2, 0.3), 50.);
	Material blue_ball(1.0, Vec4f(0.8, 0.1, 0.1, 0.0), Vec3f(0.1, 0.5, 0.9), 100.);
	Material yellow (1.0, Vec4f (0.9, 0.1, 0.0, 0.0), Vec3f(0.3, 0.4, 0.2), 10.);
	Material geg (1.0, Vec4f (0.0, 10.0, 0.8, 0.0), Vec3f(1.0, 1.0, 1.0), 1425.);
    Material red (1.0, Vec4f (0.9, 0.0, 0.0, 0.5), Vec3f(0.7, 0.3, 0.7), 10.);


    vector<Light> lights;
    lights.push_back(Light(Vec3f(-16, -7, 5), 1000));
    lights.push_back(Light(Vec3f(5.5, -6, 5), 1000));
    lights.push_back(Light(Vec3f(30, 20, 30), 6700));

    if (sceneId==1){

        vector<Sphere> spheres;
        spheres.push_back(Sphere(Vec3f(10, -2,-10) ,1.5, red));
        spheres.push_back(Sphere(Vec3f(-10, -17,-10),3, yellow));
        spheres.push_back(Sphere(Vec3f(-7, -5,-5),3, mirror));
        spheres.push_back(Sphere(Vec3f(10, -5, 10),5, pudra));
        spheres.push_back(Sphere(Vec3f(-5, -17,-8),2, blue_ball));
        spheres.push_back(Sphere(Vec3f(-25, -10,-16),4, glass));
        
        
    Model duck;
    
    render(spheres, lights, duck, outFilePath, sceneId);

    } 
    else if(sceneId==2){
        vector<Sphere> spheres;
        spheres.push_back(Sphere(Vec3f(-7, 1,-5),3, mirror));
        spheres.push_back(Sphere(Vec3f(10, 0, 10),2, pudra));
        spheres.push_back(Sphere(Vec3f(10, 1,-10) ,1, red));
        spheres.push_back(Sphere(Vec3f(-10, 1,-10),1, yellow));
        spheres.push_back(Sphere(Vec3f(-7, 1,-5),1, mirror));
        spheres.push_back(Sphere(Vec3f(10, 3, 10),1, pudra));
        spheres.push_back(Sphere(Vec3f(-5, 4,-8),1, blue_ball));
        spheres.push_back(Sphere(Vec3f(7, 5,-16),1, glass));

        Model duck("../plane.obj");
        basa(duck);

        render(spheres, lights, duck, outFilePath, sceneId);
    }
    else if(sceneId==3){
        vector<Sphere> spheres;
        spheres.push_back(Sphere(Vec3f(-7, 1,-5),3, mirror));
        spheres.push_back(Sphere(Vec3f(10, 0, 10),2, pudra));
        spheres.push_back(Sphere(Vec3f(10, 1,-10) ,1, red));
        spheres.push_back(Sphere(Vec3f(-10, 1,-10),1, yellow));
        spheres.push_back(Sphere(Vec3f(-7, 1,-5),1, mirror));
        spheres.push_back(Sphere(Vec3f(10, 3, 10),1, pudra));
        spheres.push_back(Sphere(Vec3f(-5, 4,-8),1, blue_ball));
        spheres.push_back(Sphere(Vec3f(7, 5,-16),1, glass));

        Model duck;

        render(spheres, lights, duck, outFilePath, sceneId);
    }
    return 0;
}
